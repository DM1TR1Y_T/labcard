﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*

hw: подобрать картинку, чтобы карты отображались нормально
раскладки карты: веером или рандомные (как сейчас)
забрать карту из колоды

сделать класс игрока с картами ??



 */


namespace labCard {
    public partial class Form1 : Form {
        private Random rnd = new Random();
        private Bitmap b;
        private Graphics g;
        private ImageBox imageBox;
        private CardDeck cardDeck;

        public Form1() {
            InitializeComponent();

            this.Paint += Form1_Paint;

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            imageBox = new ImageBox(Properties.Resources.card_1, 8, 14);
            //imageBox = new ImageBox(Properties.Resources.card_2, 5, 13, 13 * 5 + 3);

            cardDeck = new CardDeck(imageBox.Count);

            this.Click += (s, e) => this.Invalidate();
            this.Click += Form1_Click;
        }

        private void Form1_Click(object sender, EventArgs e) {
            DrawCardsRandom();
            this.Invalidate();
        }

        private void DrawCardsRandom() {
            
        }

        private void Form1_Paint(object sender, PaintEventArgs e) {
            //e.Graphics.DrawImage(imageBox[rnd.Next(imageBox.Count)], 0, 0);
            e.Graphics.Clear(SystemColors.Control);
            for (int i = 0; i < 8; i++) {
                /*e.Graphics.DrawImage(imageBox[cardDeck[i]], 
                    rnd.Next(this.Width - imageBox[cardDeck[i]].Width),
                    rnd.Next(this.Height - imageBox[cardDeck[i]].Height));*/
                e.Graphics.DrawImage(imageBox[i],
                    rnd.Next(this.Width - imageBox[i].Width),
                    rnd.Next(this.Height - imageBox[i].Height));
            }
        }
    }
}
